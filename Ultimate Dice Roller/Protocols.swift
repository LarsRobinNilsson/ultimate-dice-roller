//
//  Protocols.swift
//  Ultimate Dice Roller
//
//  Created by Robin Nilsson on 06/01/2019.
//  Copyright © 2019 Robin Nilsson. All rights reserved.
//

import Foundation


protocol DiceType {
    var possibleOutcomes: [[ Any ]] { get }
    var numberOfSides: Int { get }
    var hasSpecialSymbols: Bool { get }
    var commandSet: CommandSet { get }
}

extension DiceType {
    var hasSpecialSymbols: Bool { return false }
    var commandSet: CommandSet { return StandardCommands() }
    var possibleOutcomes: [[Any]] {
        var tempArray: [[Int]] = []
        for value in 1...numberOfSides { tempArray += [[value]] }
        return tempArray
    }
}


protocol CommandSet {
    func roll(numberOfDice: Int, ofType diceType: DiceType) -> [Die]
    func parseCommandSequence(_ commandSequence: String) -> ([Die], [RollDetails])
}

extension CommandSet {
    func roll(numberOfDice: Int, ofType diceType: DiceType) -> [Die] {
        var arrayOfDice: [Die] = []
        for _ in 1...numberOfDice {
            let value = Int.random(in: 1 ... diceType.numberOfSides)
            var arrayOfSymbols: [Any]? = nil
            if diceType.hasSpecialSymbols == true {
                arrayOfSymbols = diceType.possibleOutcomes[value-1]
            }
            arrayOfDice += [Die(ofType: diceType, withValueAsInteger: value, andValueAsArray: arrayOfSymbols)]
        }
        return arrayOfDice
    }
}


protocol RollDetails {}
