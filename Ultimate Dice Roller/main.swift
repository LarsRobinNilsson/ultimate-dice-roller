//
//  main.swift
//  Ultimate Dice Roller
//
//  Created by Robin Nilsson on 06/01/2019.
//  Copyright © 2019 Robin Nilsson. All rights reserved.
//

import Foundation


class Die {
    let diceType: DiceType
    let valueAsInteger: Int
    let valueAsArray: [Any]?
    var keep: Bool = true
    
    init (ofType diceType: DiceType, withValueAsInteger valueAsInteger: Int, andValueAsArray valueAsArray: [Any]?) {
        self.diceType = diceType
        self.valueAsInteger = valueAsInteger
        self.valueAsArray = valueAsArray
    }
}


class DicePool {
    let commandSet: CommandSet
    var dice: [Die] = []
    var rollDetails: [RollDetails] = []
    
    init(withCommandSet commandSet: CommandSet) {
        self.commandSet = commandSet
    }
    
    func roll(_ commandSequence: String) {
        (dice, rollDetails) = commandSet.parseCommandSequence(commandSequence)
    }
}


// Test Code //

let dicePool = DicePool(withCommandSet: StarWarsCommands() )
dicePool.roll("2 ability 1 proficiency")
