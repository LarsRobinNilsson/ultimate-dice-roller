//
//  Star Wars.swift
//  Ultimate Dice Roller
//
//  Created by Robin Nilsson on 08/01/2019.
//  Copyright © 2019 Robin Nilsson. All rights reserved.
//

import Foundation


enum StarWarsDie: DiceType { // Dice used in Star Wars: Edge of the Empire
    case ability, proficiency, boost, difficulty, challence, setback, force
    var numberOfSides: Int { return possibleOutcomes.count }
    var hasSpecialSymbols: Bool { return true }
    var commandSet: CommandSet { return StarWarsCommands() }
    
    var possibleOutcomes: [[StarWarsDiceSymbol]] {
        switch self {
        case .proficiency:
            return [[.blank], [.success], [.success], [.success, .success], [.success, .success], [.advantage], [.success, .advantage], [.success, .advantage], [.success, .advantage], [.advantage, .advantage], [.advantage, .advantage], [.triumph]]
        case .challence:
            return [[.blank], [.failure], [.failure], [.failure, .failure], [.failure, .failure], [.threat], [.threat], [.failure, .threat], [.failure, .threat], [.threat, .threat], [.threat, .threat], [.despair]]
        case .ability:
            return [[.blank], [.success], [.success], [.success, .success], [.advantage], [.advantage], [.success, .advantage], [.advantage, .advantage]]
        case .difficulty:
            return [[.blank], [.failure], [.failure, .failure], [.threat], [.threat], [.threat], [.threat, .threat], [.failure, .threat]]
        case .boost:
            return [[.blank], [.blank], [.advantage, .advantage], [.advantage], [.success, .advantage], [.success]]
        case .setback:
            return [[.blank], [.blank], [.failure], [.failure], [.threat], [.threat]]
        case .force:
            return [[.dark], [.dark], [.dark], [.dark], [.dark], [.dark], [.dark, .dark], [.light], [.light], [.light, .light], [.light, .light], [.light, .light]]
        }
    }
    
    enum StarWarsDiceSymbol {
        case success, advantage, triumph, failure, threat, despair, light, dark, blank
    }
}


struct StarWarsCommands: CommandSet { // Commands used specifically for Star Wars: Edge of the Empire
    
    func parseCommandSequence(_ commandSequence: String) -> ([Die], [RollDetails]) {
        // Add stuff to actually parse commandSequence into the following array
        let commandArray: [(number: Int, type: StarWarsDie)] = []
        var arrayOfDice: [Die] = []
        for command in commandArray {
            arrayOfDice += roll(numberOfDice: command.number, ofType: command.type)
        }
        let rollDetails = balanceTheScales(forDiceResults: arrayOfDice)
        return (arrayOfDice, rollDetails)
    }
    
    func balanceTheScales(forDiceResults diceResults: [Die]) -> [Remaining] {
        var resultingSymbols: [StarWarsDie.StarWarsDiceSymbol] = []
        for die in diceResults {
            if let symbolArray = die.valueAsArray as? [StarWarsDie.StarWarsDiceSymbol] {
                resultingSymbols += symbolArray
            }
        }
        let countOf: (triumps: Int, despair: Int) = (resultingSymbols.filter{$0 == .triumph}.count, resultingSymbols.filter{$0 == .despair}.count)
        for _ in 1 ... countOf.triumps { resultingSymbols.append(.success) }
        for _ in 1 ... countOf.despair { resultingSymbols.append(.failure) }
        compareAndRemove(.success, and: .failure, fromArray: &resultingSymbols)
        compareAndRemove(.advantage, and: .threat, fromArray: &resultingSymbols)
        return remaining(symbols: resultingSymbols)
    }
    
    func compareAndRemove(_ element1: StarWarsDie.StarWarsDiceSymbol, and element2: StarWarsDie.StarWarsDiceSymbol, fromArray array: inout [StarWarsDie.StarWarsDiceSymbol]) {
        while array.contains(where: {$0 == element1}) && array.contains(where: {$0 == element2}) {
            array.remove(at: array.index(of: element1)! )
            array.remove(at: array.index(of: element2)! )
        }
    }
    
    func remaining(symbols: [StarWarsDie.StarWarsDiceSymbol]) -> [Remaining] {
        var remaining: [Remaining] = []
        let countOf: (success: Int, advantage: Int, triumph: Int, failure: Int, threat: Int, despair: Int, light: Int, dark: Int)
            = (symbols.filter{$0 == .success}.count,
               symbols.filter{$0 == .advantage}.count,
               symbols.filter{$0 == .triumph}.count,
               symbols.filter{$0 == .failure}.count,
               symbols.filter{$0 == .threat}.count,
               symbols.filter{$0 == .despair}.count,
               symbols.filter{$0 == .light}.count,
               symbols.filter{$0 == .dark}.count)
        
        if countOf.success   > 0 { remaining.append(.successes(countOf.success)) }
        if countOf.advantage > 0 { remaining.append(.advantages(countOf.advantage)) }
        if countOf.triumph   > 0 { remaining.append(.triumphs(countOf.triumph)) }
        if countOf.failure   > 0 { remaining.append(.failures(countOf.failure)) }
        if countOf.threat    > 0 { remaining.append(.threats(countOf.threat)) }
        if countOf.despair   > 0 { remaining.append(.despair(countOf.despair)) }
        if countOf.light     > 0 { remaining.append(.light(countOf.light)) }
        if countOf.dark      > 0 { remaining.append(.dark(countOf.dark)) }
        
        return remaining
    }
    
    enum Remaining: RollDetails {
        case successes(Int)
        case advantages(Int)
        case triumphs(Int)
        case failures(Int)
        case threats(Int)
        case despair(Int)
        case light(Int)
        case dark(Int)
    }
}
