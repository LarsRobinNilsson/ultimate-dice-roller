//
//  DiceTypes.swift
//  Ultimate Dice Roller
//
//  Created by Robin Nilsson on 06/01/2019.
//  Copyright © 2019 Robin Nilsson. All rights reserved.
//

import Foundation


enum StandardDie: Int, DiceType { // Standard dice used in roleplaying games
    case d2 = 2, d4 = 4, d6 = 6, d8 = 8, d10 = 10, d12 = 12, d20 = 20, d100 = 100
    var numberOfSides: Int { return self.rawValue }
}


struct StrangelySidedDie: DiceType { // Theoretical dice with an arbitrary number of sides
    var numberOfSides: Int
    init(withNumberOfSides sides: Int) { numberOfSides = sides }
}


struct StandardCommands: CommandSet { // Standard commands used in many roleplaying games
    
    
    func parseCommandSequence(_ commandSequence: String) -> ([Die], [RollDetails]) {
        // Add stuff to actually parse commandSequence into the following array
        let commandArray: [CommandType] = []
        var dicepool: [Die] = []
        var rollDetails: [StandardRollDetails] = []
        
        for command in commandArray {
            
            switch command {
            case let .rollDice(diceCount, type): dicepool += roll(numberOfDice: diceCount, ofType: type)
            case let .keepDice(which: condition): keepDice(which: condition, fromAmong: &dicepool)
            case let .checkSuccesses(forTargetNumber: number, comparison):
                rollDetails.append( checkSuccesses(forTargetNumber: number, comparison, fromAmong: &dicepool) )
            }
        }
        return (dicepool, rollDetails)
    }
    
    func keepDice(which condition: Condition, fromAmong dicepool: inout [Die]) {
        var numberOfOutcomesToKeep: Int
        switch condition {
        case .areAmongHighest(let target):  numberOfOutcomesToKeep = target; dicepool.sort(by: { $0.valueAsInteger > $1.valueAsInteger })
        case .areAmongLowest(let target):   numberOfOutcomesToKeep = target; dicepool.sort(by: { $0.valueAsInteger < $1.valueAsInteger })
        }
        for index in 0 ..< dicepool.count {
            if index < numberOfOutcomesToKeep {
                dicepool[index].keep = true
            } else {
                dicepool[index].keep = false
            }
        }
    }
    
    func checkSuccesses(forTargetNumber number: Int, _ comparison: Comparison, fromAmong dicepool: inout [Die]) -> StandardRollDetails {
        var successes = 0
        switch comparison {
        case .andAbove: removeDice(unfavorablyComparedTo: number, comparedBy: (>=), fromAmong: &dicepool)
        case .andBelow: removeDice(unfavorablyComparedTo: number, comparedBy: (<=), fromAmong: &dicepool)
        case .exactly:  removeDice(unfavorablyComparedTo: number, comparedBy: (==), fromAmong: &dicepool)
        }
        for die in dicepool where die.keep == true { successes += 1 }
        return .successes(successes)
    }
    
    func countTotalDiceResult(fromAmong dicepool: inout [Die], andAdd modifier: Int) -> StandardRollDetails {
        var totalResult = modifier
        for die in dicepool where die.keep == true {
            totalResult += die.valueAsInteger
        }
        return .totalResult(totalResult)
    }
    
    func removeDice(unfavorablyComparedTo number: Int, comparedBy favorablyCompared: (Int, Int) -> Bool, fromAmong dicepool: inout [Die]) {
        for die in dicepool {
            if favorablyCompared(die.valueAsInteger, number) == false {
                die.keep = false
            }
        }
    }
    
    // func compare(toTargetNumber: Int) {}
    // func explode(at: Int, _: Comparison) {}
    // func checkIfSomeValuesAreIdentical() {}
    // func checkIfAllValuesAreIdentical() {}
    
    enum CommandType {
        case rollDice(Int, DiceType)
        case keepDice(which: Condition)
        case checkSuccesses(forTargetNumber: Int, Comparison)
    }
    enum StandardRollDetails: RollDetails, Equatable {
        case successes(Int)
        case totalResult(Int)
        case targetNumberReached
        case allDiceHaveSameValue
        case someDiceHaveSameValue
        
        static func == (lhs: StandardRollDetails, rhs: StandardRollDetails) -> Bool {
            switch (lhs, rhs) {
            case (.successes, .successes):      return true     // Not sure if this is needed, given the third case
            case (.totalResult, .totalResult):  return true     // Not sure if this is needed, given the third case
            case (let x, let y) where x == y:   return true
            default: return false
            }
        }
    }
    enum Condition { case areAmongHighest(Int), areAmongLowest(Int) }
    enum Comparison { case exactly, andAbove, andBelow }
}
